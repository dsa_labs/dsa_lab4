﻿#include <iostream>
#include "linked_list.h"
#include <utility>

void printMenu()
{
    printf("-----------------------------------------------------------------\n");
    printf("1. Create a list\n");
    printf("2. Add element to the front of list\n");
    printf("3. Add element to the back of list\n");
    printf("4. Remove element from the front of list\n");
    printf("5. Remove element from the back of list\n");
    printf("6. Print the list\n");
    printf("7. Destroy the list\n");
    printf("8. Exit\n");
    printf("-----------------------------------------------------------------\n");
}


int main()
{
    linkedList* list = nullptr;

    while (true)
    {
        printMenu();
        int choice;
        printf("Enter action: ");
        scanf_s("%d", &choice);
        
        switch (choice)
        {
        case 1:
        {
            list = initList();
            printf("List created\n");
            break;
        }
        case 2:
        {
            int data;
            printf("Enter data: ");
            std::cin >> data;
            list->pushFront(std::make_shared<int>(data));
            printf("Data added to front\n");
            break;
        }
        case 3:
        {
            printf("Enter data: ");
            int data;
            std::cin >> data;
			list->pushBack(std::make_shared<int>(data));
            printf("Data added to back\n");
            break;
        }
		case 4:
		{
			list->popFront();
			printf("Data removed from front\n");
			break;
		}
		case 5:
        {
            list->popBack();
			printf("Data removed from back\n");
        }
        case 6:
        {
            list->printList();
            break;
        }
		case 7:
		{
			deleteList(list);
			printf("List deleted\n");
			break;
		}
		case 8:
		{
			deleteList(list);
			return 0;
		}
		default:
			printf("Invalid action\n");
			break;
		}
    }
}